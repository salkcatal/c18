﻿#include <iostream>
#pragma
using namespace std;
class stack
{
private:
    int n;
    int* p;
    int last_index=0;
public:
    void pop();
    void push( int x);
    void print();
    int size();
    stack();
    ~stack();
};

stack::stack()
{
    n = 6;
    p = new int[n];
    for (int i = 0; i < n; i++)
        p[i] = 0;
    cout << "The array is created" << endl;
}

void stack::push(int x)
{
    if (last_index < n)
    {
        p[last_index] = x;
        last_index++;
    }
}

void stack::pop()
{
    if (last_index >0)
    {
        last_index--;
        p[last_index];
    }
}
void stack::print()
{
    for (int i = 0; i < last_index; i++)
        cout << p[i]<<' ';
    cout << endl;
}

int stack::size()
{
    return last_index;
}

stack::~stack()
{
    delete[] p;
    cout << "The array is deleted" << endl;
}

int main()
{

    stack p;
    cout << "Size of stack: ";
    cout<<p.size()<<endl;
    p.push(1);
    p.push(2);
    p.push(3);
    p.push(4);
    p.push(5);
    p.print();
    cout << "Size of stack: ";
    cout << p.size() << endl;
    p.pop();
    cout << "Size of stack after delete: ";
    cout<<p.size() << endl;
    p.print();
    
}
